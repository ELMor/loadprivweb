package es.vithas.webpriv;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;

import org.apache.poi.xwpf.converter.xhtml.XHTMLOptions;
import org.apache.poi.xwpf.converter.xhtml.XWPF2XHTMLConverter;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.jsoup.Jsoup;

public class OTs {
	public static void main(String args[]) {
		File root=new File(args[0]);
		String filename=args[0];
		File f=new File(filename);
		if(!f.isDirectory()) {
			checkFile(root,f);
		}else
			checkDir(root,f);
	}
	
	public static void checkDir(File root, File f) {
		File lf[]=f.listFiles();
		Arrays.sort(lf);
		for(File f2:lf)
			if(f2.isDirectory())
				checkDir(root,f2);
			else
				checkFile(root,f2);
	}
	
	public static void checkFile(File root, File f) {
	//	if(f.getName().toLowerCase().endsWith(".doc"))
	//		checkHWPFDoc(f);   
	//	else 
		if(f.getName().toLowerCase().endsWith(".docx"))
			checkXWPFDoc(root,f);
	}
	private static void checkXWPFDoc(File root, File f) 
	{       
		try {
			InputStream is=new FileInputStream(f);
			XWPFDocument doc=new XWPFDocument(is);
			XHTMLOptions opts=new XHTMLOptions();
			ByteArrayOutputStream baos=new ByteArrayOutputStream();
			XWPF2XHTMLConverter.getInstance().convert(doc,baos,opts);
			String result=new String(baos.toByteArray());
			String oFileName=f.getAbsolutePath();
			oFileName=oFileName.substring(0,oFileName.length()-5)+".txt";
			File oFile=new File(oFileName);
			FileOutputStream fos=new FileOutputStream(oFile);
			String in=Jsoup.parse(result).text();
			String res=procText(in);
			fos.write(res.getBytes());
			fos.close();
			System.out.print(f.getAbsolutePath().substring(root.getAbsolutePath().length())+";"+res);
		} catch (IOException e) {
			System.err.println(f.getName());
			e.printStackTrace();
		}
	}
	
	private static String procText(String in) {
		String codigo=Scanf.get(in, "Orden de Trabajo:", 1);
		String descripcion=Scanf.get(in, "Orden de Trabajo:", "Elaborado por:");
		String jornadas=Scanf.get(in, "Total Jornadas:", "Jornadas");
		return codigo+";"+descripcion+";"+jornadas+"\n";
	}
}
