package es.vithas.webpriv;

import java.util.Arrays;
import java.util.Hashtable;
import java.util.Vector;

public class TrxText {

	class Trx{
		String antes,despues;
		Hashtable<Integer, Integer> fnd;
		String fromto[][];
		
		public Trx(String in, String t[][]) {
			antes=in;
			if(t==null) {
				fromto=null;
				fnd=new Hashtable<>();
				antes=despues=in;
			}else {
				fromto=new String[t.length][2];
				for(int i=0;i<t.length;i++) 
					for(int j=0;j<2;j++) 
						fromto[i][j]=t[i][j];
				fnd=find(in);
				despues=replaceForward(fnd, in);
			}
		}
		public Hashtable<Integer, Integer> find(String contenido) {
			Hashtable<Integer, Integer> ret=new Hashtable<>();
			for(int i=0;i<fromto.length;i++){
				String search=fromto[i][0];
				int pos=-search.length();
				for(;;) {
					pos=contenido.indexOf(search,pos+search.length());
					if(pos<0)
						break;
					ret.put(pos, i);
				}
			}
			return ret;
		}
		public String replaceForward(Hashtable<Integer, Integer> fnd, String ori) {
			int delta=0;
			String ret=ori;
			Integer positions[]=fnd.keySet().toArray(new Integer[fnd.size()]);
			Arrays.sort(positions);
			for(int index=0;index<positions.length;index++) {
				int trxnum=fnd.get(positions[index]);
				String from=fromto[trxnum][0],to=fromto[trxnum][1];
				String tram1=ret.substring(0, positions[index]+delta);
				String tram2=ret.substring(positions[index]+delta+from.length());
				ret=tram1+to+tram2;
				delta+=to.length()-from.length();
			}
			return ret;
		}
		public String replaceBackward(Hashtable<Integer, Integer> fnd, String ori) {
			String ret=ori;
			Integer positions[]=fnd.keySet().toArray(new Integer[fnd.size()]);
			Arrays.sort(positions);
			int delta=0;
			for(int pos:positions)
				delta+=fromto[fnd.get(pos)][1].length()-fromto[fnd.get(pos)][0].length();
			for(int index=positions.length-1;index>-1;index--) {
				int trxnum=fnd.get(positions[index]);
				String from=fromto[trxnum][0],to=fromto[trxnum][1];
				String tram1=ret.substring(0, positions[index]+delta+from.length()-to.length());
				String tram2=ret.substring(positions[index]+delta+from.length());
				ret=tram1+from+tram2;
				delta-=to.length()-from.length();
			}
			return ret;
		}
		public String getProcessed() {
			return despues;
		}
		public String getOriginal() {
			return antes;
		}
		public int cvtBackCoord(int x) {
			if(fnd.size()==0) //No hay cambios de longitud entre antes y despues
				return x;
			Integer positions[]=fnd.keySet().toArray(new Integer[fnd.size()]);
			Arrays.sort(positions);
			if(x<=positions[0]) //x está antes del primer cambio
				return x;
			int posAntes=positions[0];
			int posDespues=positions[0];
			int posNdx=0;
			while(x<posDespues && posNdx<positions.length) {
				int rule=fnd.get(positions[posNdx]);
				posAntes=fromto[rule][0].length();
				posDespues=fromto[rule][1].length();
			}
			return posAntes+(x-posDespues);
		}
	}
	
	Vector<Trx> trxPath=new Vector<>();
	public TrxText(String contenido) {
		trxPath.add(new Trx(contenido, null));
	}
	
	public void apply(String t[][]) {
		String current=trxPath.lastElement().despues;
		trxPath.add(new Trx(current,t));
	}
	
	public String antes() {
		return trxPath.lastElement().antes;
	}
	public String despues() {
		return trxPath.lastElement().despues;
	}
	public int backCoord(int x) {
		return trxPath.lastElement().cvtBackCoord(x);
	}
	public String backString(int x, int y) {
		Trx t=trxPath.lastElement();
		return t.antes.substring(t.cvtBackCoord(x),t.cvtBackCoord(y));
	}
	public String backString(int x) {
		Trx t=trxPath.lastElement();
		return t.antes.substring(t.cvtBackCoord(x));
	}
}
